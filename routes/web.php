<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HighlightController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', function () {
    return redirect()->route('login');
})->name("home");

Route::get('/profile', function () {
    return view('profile_bar');
});
// Route::get('/login', function () {
//     return view('login');
// });

Auth::routes();

Route::get('/dashboard/{nickname}', [HomeController::class, 'index'])->name('dashboard');
Route::put('/update-profile/{user}', [HomeController::class, 'update'])->name('update-profile');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// });

// Route::get('/post', function () {
//     return view('post');
// });

// postingan
Route::get('/photo/{nickname}', [PostController::class, 'showPhotos'])->name('photos');
Route::get('/video/{nickname}', [PostController::class, 'showVideos'])->name('videos');


Route::post('/store-post', [PostController::class, 'store'])->name('store-post');
Route::put('/update-post/{post}', [PostController::class, 'update'])->name('update-post');
Route::delete('/delete/{post}', [PostController::class, 'destroy'])->name('delete');

Route::post('/store-highlight', [HighlightController::class, 'store'])->name('store-highlight');
Route::put('/update-highlight/{highlight}', [HighlightController::class, 'update'])->name('highlights.update');
Route::delete('/delete-highlight/{highlight}', [HighlightController::class, 'destroy'])->name('highlights.destroy');
