<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/login.css') }}" />
    <link rel="icon" href="{{ asset('img/MAIPRO.png') }}" type="image/icon type">
    <title>Clone Instagram</title>
    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('img/PWA-MAIPRO.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
</head>

<body>
    <div class="container">
        <div class="forms-container">
            <div class="signin-signup">
                {{-- LOGIN --}}
                <form method="POST" action="{{ route('login') }}" class="sign-in-form">
                    @csrf
                    <h2 class="title">Sign in</h2>
                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" placeholder="Email" name="email" required autocomplete="off" />
                    </div>
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" placeholder="Password" name="password" required autocomplete="off" />
                    </div>
                    <input type="submit" value="Login" class="btn solid" />
                    <p class="social-text">Or Sign in with social platforms</p>
                    <div class="social-media">

                        <a href="#" class="social-icon">
                            <i class="fab fa-google"></i>
                        </a>

                    </div>
                </form>

                {{-- REGISTER --}}
                <form method="POST" action="{{ route('register') }}" class="sign-up-form">
                    @csrf

                    <h2 class="title">Sign up</h2>

                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input for="nickname" id="nickname" type="text" placeholder="Nickname"
                            class="@error('nickname') is-invalid @enderror" name="nickname"
                            value="{{ old('nickname') }}" required autocomplete="off" autofocus>
                        @error('nickname')
                            <span class="invalid-feedback" role="alert">
                                <strong>silahkan isi dengan huruf kecil "a-z"</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input for="name" id="name" type="text" placeholder="Name"
                            class="@error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"
                            required autocomplete="off" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-field">
                        <i class="fas fa-suitcase"></i>
                        <input for="job" id="job" type="text" placeholder="Job"
                            class="@error('job') is-invalid @enderror" name="job" value="{{ old('job') }}"
                            required autocomplete="off" autofocus>
                        @error('job')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-field">
                        <i class="fas fa-envelope"></i>
                        <input for="email" id="email" type="email" placeholder="Email"
                            class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                            required autocomplete="off">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input for="password" id="password" type="password" placeholder="Password"
                            class="@error('password') is-invalid @enderror" name="password" required
                            autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input id="password-confirm" type="password" name="password_confirmation" required
                            autocomplete="new-password"placeholder="Confirm Password" />
                    </div>


                    <input type="submit" class="btn" value="Sign up" />
                    <p class="social-text">Or Sign up with social platforms</p>
                    <div class="social-media">
                        <a href="#" class="social-icon">
                            <i class="fab fa-google"></i>
                        </a>
                    </div>
                </form>
            </div>
        </div>

        <div class="panels-container">
            <div class="panel left-panel">
                <div class="content">
                    <h3>MAIPRO</h3>
                    <p>
                        Aplikasi Profile Karyawan yang digunakan untuk menampilkan profile beserta kegiatan karyawan
                        seperti media sosial
                    </p>
                    <button class="btn transparent" id="sign-up-btn">
                        Sign up
                    </button>
                </div>
                <div class="js-tilt" data-tilt="">
                    <img src="{{ asset('img/MAIPRO2.png') }}" class="image" alt="" />
                </div>
            </div>
            <div class="panel right-panel">
                <div class="content">
                    <h3>One of us ?</h3>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
                        laboriosam ad deleniti.
                    </p>
                    <button class="btn transparent" id="sign-in-btn">
                        Sign in
                    </button>
                </div>
                <div class="js-tilt" data-tilt="">
                    <img src="{{ asset('img/MAIPRO2.png') }}" class="image" alt="" />
                </div>
            </div>
        </div>
    </div>

    {{-- Animasi --}}
    <script src="{{ asset('js/jquery-3.2.1.min-1.js') }}"></script>
    <script src="{{ asset('js/login.js') }}"></script>
    <script src="{{ asset('js/tilt.jquery.min-1.js') }}"></script>

    <script>
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
    {{-- PWA --}}
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>


</body>

</html>
