<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clone Instagram</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <link rel="icon" href="{{ asset('img/MAIPRO.png') }}" type="image/icon type">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/af689195eb.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('img/PWA-MAIPRO.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
</head>

<body>

    {{-- MODAL EDIT --}}
    <div class="modal fade" id="modal-edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('update-profile', $profile->id) }}" method="post" id="edit-form-modal"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Edit Profile
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row h-100">
                            <div class="col-12">
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="Nickname">Nickname</label>
                                    <input type="text" name="nickname" class="form-control form-control-md pb-0"
                                        id="Nickname" rows="2" placeholder="Type a nickname for profile"
                                        value="{{ $profile->nickname }}">
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="Name">Name</label>
                                    <input type="text" name="name" class="form-control form-control-md pb-0"
                                        id="Name" rows="2" placeholder="Type a name for profile"
                                        value="{{ $profile->name }}">
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="PostImageProfile">Input
                                        Image Profile</label>
                                    <input name="photo" type="file" id="PostImageProfile"
                                        class="form-control form-control-md" />
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="PostImageBackground">Input
                                        Image Background</label>
                                    <input name="background" type="file" id="PostImageBackground"
                                        class="form-control form-control-md" />
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="Job">Job</label>
                                    <input type="text" name="job" class="form-control form-control-md pb-0"
                                        id="Job" rows="1" placeholder="Type a job for profile"
                                        value="{{ $profile->job }}">
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="Bio">Bio</label>
                                    <input type="text" name="bio" class="form-control form-control-md pb-0"
                                        id="Bio" rows="4" placeholder="Type a bio for profile"
                                        value="{{ $profile->bio }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success px-4" form="edit-form-modal">Edit</button>
                    </div>
                </form>'
            </div>
        </div>
    </div>
    {{-- END MODAL EDIT --}}

    {{-- MODAL HIGHLIGHT --}}
    <div class="modal fade" id="addHighlight" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('store-highlight') }}" method="post" id="addHighlight-form"
                    enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="is_video" value="0">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Upload New Highlight</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row h-100">
                            <div class="col-lg-7 col-12 mb-3 mb-lg-0">
                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center border border-1 border-secondary rounded preview-placeholder">
                                    <div class="text-center">
                                        <i class="fs-1 text-muted fa-regular fa-image"></i>
                                        <h6 class="text-muted pt-2">Image Preview</h6>
                                    </div>
                                </div>
                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview-img">
                                    <img class="w-100 h-100" src=".." alt="Post1"
                                        style="max-height: 40rem; object-fit: contain">
                                </div>
                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview-vid">
                                    <video class="w-100 h-100" controls
                                        style="max-height: 40rem; object-fit: contain">
                                        <source src="...">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            </div>
                            <div class="col-lg-5 col-12">

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typeHighlightFileX-2">Input
                                        Image</label>
                                    <input name="thumbnail" type="file" id="typeHighlightFileX-2"
                                        class="form-control form-control-md" />
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typeTitleX-2">Title</label>
                                    <textarea name="title" class="form-control form-control-md pb-0" id="typeTitleX-2" rows="3"
                                        placeholder="Type a title for new post"></textarea>
                                </div>

                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typeDescriptionX-2">Desc</label>
                                    <textarea name="desc" class="form-control form-control-md pb-0" id="typeDescriptionX-2" rows="3"
                                        placeholder="Type a title for new post"></textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success px-4"
                            id="addHighlight-form-btn">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- MODAL HIGHLIGHT END --}}

    {{-- MODAL ITEM HIGHLIGHT --}}
    <div class="modal fade" id="highlightItem" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header pb-0" style="border-bottom: none;">
                    <div class="d-flex flex-wrap w-100 justify-content-between">
                        <div class="col-11 d-flex">
                            <img class="highlight-prof-pic post-profil overflow-hidden rounded-circle" src=""
                                alt="Denas" style="object-fit: cover;">
                            <div class="ms-3">
                                <h5 class="pb-0 mb-0 highlight-user-name"></h5>
                                <p class="pt-0 mt-0 fw-light highlight-user-time"></p>
                            </div>
                        </div>
                        <div class="col-1 d-flex align-items-start justify-content-end">
                            @auth
                                @if (Auth::id() == $profile->id)
                                    <div class="btn-group dropstart">
                                        <button type="button" data-bs-toggle="dropdown" aria-expanded="false"
                                            class="border-0 bg-transparent highlight-option">
                                            <i class="fa-solid fa-ellipsis-vertical"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="dropdown-item edit-highlight-btn text-purple"
                                                    href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#editHighlight" data-title="" data-desc="">
                                                    <i class="fa-solid fa-pen pe-2"></i> Edit this Post
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item delete-highlight-btn text-purple" href="#">
                                                    <i class="fa-solid fa-trash pe-2"></i> Delete this Post
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            @endauth
                        </div>
                    </div>

                </div>
                <div class="modal-body p-0">
                    <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                        <img class="w-100 h-100 highlight-file-img d-none" src="" alt="Highlight"
                            style="max-height: 40rem; object-fit: contain">
                        <video class="w-100 h-100 highlight-file-video d-none"
                            style="max-height: 40rem; object-fit: contain" controls>
                            <source src="">
                            Your browser does not support HTML video.
                        </video>
                    </div>
                </div>
                <div class="modal-footer justify-content-start" style="border-top: none;">
                    <p class="highlight-description"></p>
                </div>
            </div>
        </div>
    </div>
    <form action="" method="POST" id="delete-form">
        @csrf
        @method('DELETE')
    </form>
    {{-- MODAL ITEM HIGHLIGHT END --}}

    {{-- MODAL FOTO --}}
    <div class="modal fade" id="addPhoto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('store-post') }}" method="post" id="addPhoto-form"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="is_video" value="0">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Upload New Post Photo</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row h-100">
                            <div class="col-lg-7 col-12 mb-3 mb-lg-0">
                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center border border-1 border-secondary rounded preview-placeholder">
                                    <div class="text-center">
                                        <i class="fs-1 text-muted fa-regular fa-image"></i>
                                        <h6 class="text-muted pt-2">Image Preview</h6>
                                    </div>
                                </div>

                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview">
                                    <img class="w-100 h-100" src=".." alt="Post1"
                                        style="max-height: 40rem; object-fit: contain">
                                </div>
                            </div>
                            <div class="col-lg-5 col-12">
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typePostImageX-2">Input
                                        Image</label>
                                    <input name="thumbnail" type="file" id="typePostImageX-2"
                                        class="form-control form-control-md" />
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typeDescriptionX-2">Description</label>
                                    <textarea name="desc" class="form-control form-control-md pb-0" id="typeDescriptionX-2" rows="10"
                                        placeholder="Type a description for new post"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success px-4" id="addPhoto-form-btn">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- MODAL FOTO END --}}

    {{-- MODAL VIDEO --}}
    <div class="modal fade" id="addVideo" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">

                <form action="{{ route('store-post') }}" method="post" id="addVideo-form"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="is_video" value="1">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Upload New Post Video</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row h-100">
                            <div class="col-lg-7 col-12 mb-3 mb-lg-0">
                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center border border-1 border-secondary rounded preview-placeholder">
                                    <div class="text-center">
                                        <i class="fs-1 text-muted fa-solid fa-video"></i>
                                        <h6 class="text-muted pt-2">video Preview</h6>
                                    </div>
                                </div>

                                <div
                                    class="w-100 h-100 d-flex align-items-center justify-content-center d-none preview">
                                    {{-- <img class="w-100 h-100" src=".." alt="Post1"
                                                style="max-height: 40rem; object-fit: contain"> --}}
                                    <video class="w-100 h-100" controls
                                        style="max-height: 27rem; object-fit: contain">
                                        <source src="...">
                                        Your browser does not support the video tag.
                                    </video>
                                </div>

                            </div>
                            <div class="col-lg-5 col-12">
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typePostVideoX-2">Input
                                        video</label>
                                    <input name="thumbnail" type="file" id="typePostVideoX-2"
                                        class="form-control form-control-md" />
                                </div>
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="typeDescriptionX-2">Description</label>
                                    <textarea name="desc" class="form-control form-control-md pb-0" id="typeDescriptionX-2" rows="10"
                                        placeholder="Type a description for new post"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success px-4" id="addVideo-form-btn">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- MODAL VIDEO END --}}

    {{-- MODAL QRCODE --}}
    <div class="modal fade" id="Qrcode" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none;">
                    <h5 class="modal-title" id="staticBackdropLabel">QrCode</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body text-center modal-qrcode">
                    {!! QrCode::size(300)->generate(Request::url()) !!}
                </div>
                <div class="modal-footer justify-content-center pb-lg-4 pb-3" style="border-top: none;">
                    <a href="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(500)->generate(Request::url())) }}"
                        class="btn btn-success" download="qrcode">Download</a>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL QRCODE END --}}

    {{-- NAVBAR --}}
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('img/MAIPRO1.png') }}" class="navbar-logo" alt="">
            </a>
            <nav class="navbar navbar-expand-lg bg-light d-none d-md-block">
                <div class="container-fluid">
                    <div class=" navbar-collapse collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav">
                            @auth
                                <div class="dropdown d-flex justify-content-center">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link active dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            New Post
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" aria-current="page" href="#"
                                                    data-bs-toggle="modal" data-bs-target="#addPhoto">Upload Photo</a>
                                            </li>
                                            <li><a class="dropdown-item" aria-current="page" href="#"
                                                    data-bs-toggle="modal" data-bs-target="#addVideo">Upload Video</a>
                                            </li>
                                        </ul>
                                    </li>
                                </div>
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#" data-bs-toggle="modal"
                                        data-bs-target="#Qrcode">Qr Code</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">Night Mode</a>
                                </li>
                            @else
                                <a class="nav-link active" aria-current="page" href="#">Night Mode</a>
                                <div class="dropdown">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link active dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            User
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" aria-current="page"
                                                    href="{{ route('login') }}">Login</a>
                                            </li>
                                        </ul>
                                    </li>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </nav>

            <nav class="navbar navbar-expand-lg bg-light d-none d-md-block">
                <div class="container-fluid">
                    <div class=" navbar-collapse collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav">
                            @auth
                                <div class="dropdown">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link active dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Account
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-end">
                                            <li>
                                                <a class="dropdown-item" href="#1" data-bs-toggle="modal"
                                                    data-bs-target="#modal-edit"
                                                    data-href="{{ route('update-profile', $profile->nickname) }}">
                                                    <i class="fa-solid fa-pen pe-2"></i> Edit Profile
                                                </a>
                                            </li>
                                            <li>
                                                <a class="dropdown-item" href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                    class="d-none">
                                                    @csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                </div>
                            @else
                                <a class="nav-link active" aria-current="page" href="#">Night Mode</a>
                                <div class="dropdown">
                                    <li class="nav-item dropdown">
                                        <a class="nav-link active dropdown-toggle" href="#" role="button"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            User
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" aria-current="page"
                                                    href="{{ route('login') }}">Login</a>
                                            </li>
                                        </ul>
                                    </li>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </nav>




            {{-- NAVBAR BOTTOM --}}
            <div class="p-0 fixed-bottom navbar-expand d-md-none d-lg-none d-xl-none navbar-light bg-info border-top">
                <ul class="navbar-nav my-2  nav-justified w-100">
                    @auth
                        <li class="nav-item d-flex justify-content-center align-items-center dropup">
                            <a class="nav-link active" href="#" role="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="fa-solid fa-square-plus fs-1 change"></i>
                                <p class="mb-0 change" style="font-size: 13px">NewPost</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" aria-current="page" href="#" data-bs-toggle="modal"
                                        data-bs-target="#addPhoto">Upload Photo</a></li>
                                <li><a class="dropdown-item" aria-current="page" href="#" data-bs-toggle="modal"
                                        data-bs-target="#addVideo">Upload Video</a></li>
                            </ul>
                        </li>
                        <li class="nav-item d-flex justify-content-center align-items-center">
                            <a class="nav-link active" aria-current="page" href="#" data-bs-toggle="modal"
                                data-bs-target="#Qrcode">
                                <i class="fa-solid fa-qrcode fs-1 change"></i>
                                <p class="mb-0 change" style="font-size: 13px">Qr Code</p>
                            </a>
                        </li>
                        <li class="nav-item d-flex justify-content-center align-items-center">
                            <a class="nav-link active" aria-current="page" href="#"><i
                                    class="fa-solid fa-moon fs-1"></i>
                                <p class="mb-0" style="font-size: 13px">NightMode</p>
                            </a>
                        </li>
                        <li class="nav-item d-flex justify-content-center align-items-center dropup">
                            <a class="nav-link active" href="#" role="button" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fa-solid fa-user fs-1"></i>
                                <p class="mb-0" style="font-size: 13px">Account</p>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuProfile">
                                <li>
                                    <a class="dropdown-item" href="#">{{ __('Edit Profil') }}</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        class="d-none">
                                        @csrf
                                    </form>
                                </li>

                            </ul>
                        </li>
                    @else
                        <li class="nav-item d-flex justify-content-center align-items-center">
                            <a class="nav-link active" aria-current="page" href="#"><i
                                    class="fa-solid fa-moon fs-1"></i>
                                <p class="mb-0" style="font-size: 13px">NightMode</p>
                            </a>
                        </li>
                        <li class="nav-item d-flex justify-content-center align-items-center dropup">
                            <a class="nav-link active" href="#" role="button" data-bs-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fa-solid fa-user fs-1"></i>
                                <p class="mb-0" style="font-size: 13px">User</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" aria-current="page" href="{{ route('login') }}">Login</a>
                                </li>
                            </ul>
                        </li>
                    @endauth

                </ul>
            </div>
        </div>
    </nav>
    {{-- END NAVBAR --}}

    <main>
        {{-- new header profile --}}
        <div class="container-xl px-0" style="overflow: hidden">
            <!-- Column -->
            <div class="card" style="border-radius: 10px">
                <img class="card-img-top border-0 rounded-0" style=" object-fit: cover;"
                    src="{{ asset("img/profiles/$profile->background") }}">
                <div class="card-body little-profile text-center">
                    <div class="pro-img"><img src="{{ asset("img/profiles/$profile->photo") }}" alt="user">
                    </div>
                    <div class="teksdeskripsi">
                        <h2 class="mb-0" style="font-weight: 700; font-size:20px;">{{ $profile->name }}</h2>
                        <h6 class="py-2" style="font-size: 14px; color: #fc6453;">{{ $profile->job }}</h6>
                        <p style="padding-left: 5vw; padding-right:5vw; font-size: 14px; font-weight: 400;">
                            {{ $profile->bio }}</p>
                    </div>
                </div>
            </div>


            {{-- POST HIGHLIGHT --}}
            <div class="splide" role="group" aria-label="Splide Basic HTML Example">
                <div class="container highlight text-center splide__track px-0 ms-0">
                    <div class="d-flex  text-center position-relative top-50 start-0 splide__list">
                        @auth
                            <div class="d-flex flex-column justify-content-center splide__slide"
                                style="width: auto !important">
                                <a href="#" class="">
                                    <div class="d-flex justify-content-center ">
                                        <a class="bg-rounded-circle bg-white position-relative" style="object-fit: cover"
                                            href="#" aria-current="page" href="#" data-bs-toggle="modal"
                                            data-bs-target="#addHighlight"><i
                                                class="fa-solid fa-plus position-absolute top-50 start-50 translate-middle"></i>
                                        </a>
                                    </div>

                                    <div class="teksdeskripsi2 d-flex justify-content-center">
                                        <p
                                            style="white-space: nowrap; width: 100px; overflow: hidden; text-overflow: ellipsis;">
                                            Add</p>
                                    </div>
                                </a>
                            </div>
                        @endauth


                        @foreach ($posthighlight as $item)
                            <div class="mx-2 d-flex justify-content-center splide__slide">
                                <a href="#" class="" data-bs-toggle="modal"
                                    data-bs-target="#highlightItem"
                                    data-href-edit="{{ route('highlights.update', $item->id) }}"
                                    data-href-delete="{{ route('highlights.destroy', $item->id) }}"
                                    data-file="{{ asset("img/highlights/$item->thumbnail") }}"
                                    data-title="{{ $item->title }}"
                                    data-time="{{ $item->created_at->format('d F Y') }}"
                                    data-desc="{{ $item->desc }}" data-is-video="{{ $item->is_video }}"
                                    data-user-photo="{{ asset('img/profiles/' . $item->user->photo) }}"
                                    data-user-name="{{ $item->user->name }}" data-user-job="{{ $item->user->job }}">
                                    <div class="d-flex justify-content-center position-relative">
                                        @if ($item->is_video)
                                            <video class="bg-rounded-circle" nocontrols style="object-fit: cover">
                                                <source src="{{ asset('img/highlights/' . $item->thumbnail) }}">
                                                Your browser does not support the video tag.
                                            </video>
                                        @else
                                            <img class="bg-rounded-circle"
                                                src="{{ asset('img/highlights/' . $item->thumbnail) }}"
                                                alt="fotohighlight" style="object-fit: cover">
                                        @endif

                                    </div>
                                    <div class="teksdeskripsi2">
                                        <p
                                            style="white-space: nowrap; width: 100px; overflow: hidden; text-overflow: ellipsis;">
                                            {{ $item->title }}</p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            {{-- postingan --}}
            <div class="card" style="border-radius: 10px; margin-bottom:80px">
                <div class="row g-0 text-center mt-2 ">
                    <div class="col-6 col-sm-6 col-md-6 btn btn-active hover-option" id="postfoto"><i
                            class="fa-sharp fa-solid fa-grip"></i>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 btn hover-option " id="postvideo"><i
                            class="fa-solid fa-play"></i>
                    </div>

                    {{-- post foto --}}
                    <div class="col-xl-12 col-md-12">
                        <div class="tab-content">
                            <div id="home" class="tab-pane active">
                                <div class="body-post d-flex flex-wrap" id="postingan-foto">

                                    @foreach ($postfoto as $item)
                                        <div class="col-4 p-1 col-sm-4 col-md-3 col-lg-2">
                                            <div class="position-relative w-100" style="padding-top: 100%;">
                                                <div
                                                    class="w-100 h-100 position-absolute top-50 start-50 translate-middle">
                                                    <a href="{{ route('photos', $profile->nickname) }}/#post{{ $item->id }}"
                                                        class="text-decoration-none">
                                                        <div
                                                            class="w-100 h-100 position-absolute top-50 start-50 translate-middle">
                                                        </div>
                                                        <img class="w-100 h-100"
                                                            src="{{ asset('img/posts/' . $item->thumbnail) }}"
                                                            alt="photo" style="object-fit: cover">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="body-post flex-wrap">
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- post video --}}
                    <div class="col-xl-12 col-md-12">
                        <div class="tab-content">
                            <div id="home" class="tab-pane active">
                                <div class="body-post d-flex flex-wrap d-none " id="postingan-video">

                                    @foreach ($postvideo as $item)
                                        <div class="col-4 p-1 col-sm-4 col-md-3 col-lg-2">
                                            <div class="gallery-item position-relative w-100"
                                                style="padding-top: 150%;">
                                                <div
                                                    class="w-100 h-100 position-absolute top-50 start-50 translate-middle ">
                                                    <a href="{{ route('videos', $profile->nickname) }}/#post{{ $item->id }}"
                                                        class="text-decoration-none">
                                                        <div
                                                            class="w-100 h-100 position-absolute top-50 start-50 translate-middle">
                                                        </div>
                                                        <video class="gallery-image w-100 h-100" nocontrols
                                                            style="object-fit: cover">
                                                            <source
                                                                src="{{ asset('img/posts/' . $item->thumbnail) }}">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                        <div class="gallery-item-type">
                                                            <span class="visually-hidden"></span><i
                                                                class="fa-solid fa-clapperboard text-white"
                                                                aria-hidden="true"></i>
                                                        </div>
                                                        <div class="gallery-item-info">
                                                            <ul>
                                                                <li class="gallery-item-likes"><i
                                                                        class="fa-solid fa-play text-white"
                                                                        aria-hidden="true"></i></li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <div class="body-post flex-wrap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    {{-- script --}}
    {{-- bootstrap --}}
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    {{-- fontawesome --}}
    <script src="https://kit.fontawesome.com/af689195eb.js" crossorigin="anonymous"></script>

    {{-- jquery --}}
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

    {{-- Splide JS CDN --}}
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.2/dist/js/splide.min.js"></script>

    {{-- <script>
        function toggleActive() {
            var element = document.getElementById("postfoto");
            element.classList.toggle("btn-active");
        }
    </script> --}}

    <script>
        $("#postfoto").click(function() {
            $("#postfoto").addClass("btn-active");
            $("#postvideo").removeClass("btn-active");
            $("#postingan-video").addClass("d-none");
            $("#postingan-foto").removeClass("d-none");
        });
        $("#postvideo").click(function() {
            $("#postvideo").addClass("btn-active");
            $("#postfoto").removeClass("btn-active");
            $("#postingan-foto").addClass("d-none");
            $("#postingan-video").removeClass("d-none");
        });
    </script>

    {{-- Splide --}}
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.2/dist/js/splide.min.js"></script>
    <script>
        $(document).ready(function() {
            var splide = new Splide('.splide', {
                drag: 'free',
                snap: true,
                pagination: false,
                arrows: false,
                padding: '5rem',
                perPage: 8,
                breakpoints: {
                    1400: {
                        perPage: 7,
                    },
                    992: {
                        perPage: 6,
                    },
                    768: {
                        perPage: 4,
                    },
                    576: {
                        perPage: 2,
                    },
                },
            });

            splide.mount();
        });
    </script>

    {{-- image preview --}}
    {{-- <script>
        filepost.onchange = evt => {
            const [file] = filepost.files
            if (file) {
                preview.src = URL.createObjectURL(file)
            }
        }
    </script> --}}

    {{-- <script>
        $('.nav-link').click(function() {
            $(".change").css("color", "yellow");
        })
    </script> --}}


    {{-- JS MODAL CRUD --}}
    <Script>
        const image_extension = ['png', 'jpg', 'jpeg', 'webp', 'gif', 'svg'];
        const video_extension = ['mp4'];
        const file_extension = image_extension.concat(video_extension);

        // {{-- JS PREVIEW VIDEO --}}
        $('#typePostVideoX-2').on("change", function(event) {
            let fileName = event.target.value;
            let extension = fileName.substring(fileName.lastIndexOf('.') + 1);

            let validation = video_extension.findIndex(element => {
                if (element.toLowerCase().includes(extension.toLowerCase())) {
                    return true;
                }
            });

            if (validation !== -1) {
                $(this).closest('.modal-body').find('.preview-placeholder').addClass("d-none");
                $(this).closest('.modal-body').find('.preview').removeClass("d-none");
                $(".preview > video > source").attr("src", URL.createObjectURL(event.target.files[0]));
                $(".preview > video")[0].load();
            }
        });

        // {{-- JS PREVIEW IMAGE --}}
        $('#typePostImageX-2').on("change", function(event) {
            let element = $(this).closest('.modal');
            let file = element.find('#typePostImageX-2')[0].files[0];
            let fileName = element.find('#typePostImageX-2')[0].value;
            let extension = fileName.substring(fileName.lastIndexOf('.') + 1);
            let reader = new FileReader();

            let validation = image_extension.findIndex(element => {
                if (element.toLowerCase().includes(extension.toLowerCase())) {
                    return true;
                }
            });

            if (validation !== -1) {
                reader.onloadend = function() {
                    element.find('.preview > img').attr("src", reader.result);
                }

                if (file) {
                    reader.readAsDataURL(file);
                    element.find('.preview-placeholder').addClass("d-none");
                    element.find('.preview').removeClass("d-none");
                } else {
                    // element.find('.profile-img-preview').attr("src", "");
                    element.find('.preview > img').attr("src", "");
                }
            }
        });

        // {{-- JS PREVIEW HIGHLIGHT --}}
        $('#typeHighlightFileX-2').on("change", function(event) {
            let element = $(this).closest('.modal');
            let file = element.find('#typeHighlightFileX-2')[0].files[0];
            let fileName = element.find('#typeHighlightFileX-2')[0].value;
            let extension = fileName.substring(fileName.lastIndexOf('.') + 1);

            let validation = file_extension.findIndex(element => {
                if (element.toLowerCase().includes(extension.toLowerCase())) {
                    return true;
                }
            });

            let image_validation = image_extension.findIndex(element => {
                if (element.toLowerCase().includes(extension.toLowerCase())) {
                    return true;
                }
            });

            let video_validation = video_extension.findIndex(element => {
                if (element.toLowerCase().includes(extension.toLowerCase())) {
                    return true;
                }
            });

            if (validation !== -1) {
                if (image_validation !== -1) {
                    let reader = new FileReader();
                    reader.onloadend = function() {
                        element.find('.preview-img > img').attr("src", reader.result);
                    }
                    if (file) {
                        reader.readAsDataURL(file);
                        element.find('.preview-placeholder').addClass("d-none");
                        element.find('.preview-vid').addClass("d-none");
                        element.find('.preview-img').removeClass("d-none");
                    } else {
                        element.find('.preview-img > img').attr("src", "");
                    }
                } else if (video_validation !== -1) {
                    element.find('.preview-placeholder').addClass("d-none");
                    element.find('.preview-img').addClass("d-none");
                    element.find('.preview-vid').removeClass("d-none");
                    $(".preview-vid > video > source").attr("src", URL.createObjectURL(event.target
                        .files[
                            0]));
                    $(".preview-vid > video")[0].load();
                }
            }
        });
    </script>

    {{-- JS HIGHLIGHT --}}
    <script>
        $('#highlightItem').on('show.bs.modal', event => {
            // Button that triggered the modal
            const button = event.relatedTarget;

            // Data in Triggered Button
            const edit_href = button.getAttribute('data-href-edit');
            const delete_href = button.getAttribute('data-href-delete');
            const file = button.getAttribute('data-file');
            const title = button.getAttribute('data-title');
            const time = button.getAttribute('data-time');
            const desc = button.getAttribute('data-desc');
            const is_video = button.getAttribute('data-is-video');
            const user_photo = button.getAttribute('data-user-photo');
            const user_name = button.getAttribute('data-user-name');
            const user_job = button.getAttribute('data-user-job');

            // Update the modal's content.
            $('.highlight-prof-pic').attr('src', user_photo);
            $('.highlight-user-name').html(user_name +
                ' <span class="fs-6 fw-light highlight-user-job d-none d-sm-inline-flex"> | ' +
                user_job + '</span>');
            $('.highlight-user-time').html(time);
            $('.highlight-description').html('<b>' + user_name + '</b> - ' + desc);
            $('.edit-highlight-btn').attr('data-href', edit_href);
            $('.edit-highlight-btn').attr('data-title', title);
            $('.edit-highlight-btn').attr('data-desc', desc);
            $('.delete-highlight-btn').attr('href', delete_href);

            if (is_video == true) {
                $('.highlight-file-img').addClass('d-none');
                $('.highlight-file-video').removeClass('d-none');
                $('.highlight-file-video > source').attr('src', file);
                $('.highlight-file-video')[0].load();
            } else {
                $('.highlight-file-video').addClass('d-none');
                $('.highlight-file-img').removeClass('d-none');
                $('.highlight-file-img').attr('src', file);
            }
        });
    </script>

    <script>
        $(".delete-highlight-btn").on("click", function(event) {
            event.preventDefault();

            const href = $(this).attr('href');
            const form = $('#delete-form');

            form.attr('action', href)

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-md btn-success mx-2',
                    cancelButton: 'btn btn-md btn-danger mx-2',
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    swalWithBootstrapButtons.fire({
                        title: 'Deleted!',
                        text: 'Your highlight has been deleted.',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    form.submit();
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire({
                        title: 'Cancelled',
                        text: 'Your highlight is safe :v',
                        icon: 'error',
                        showConfirmButton: false,
                        timer: 2000,
                    })
                }
            });
        });
    </script>

    {{-- PWA --}}
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
</body>


</html>
