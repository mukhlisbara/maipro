<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Clone Instagram</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/post.css') }}">
    <link rel="icon" href="{{ asset('img/MAIPRO.png') }}" type="image/icon type">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <script src="https://kit.fontawesome.com/af689195eb.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- PWA  -->
    <meta name="theme-color" content="#6777ef" />
    <link rel="apple-touch-icon" href="{{ asset('img/PWA-MAIPRO.png') }}">
    <link rel="manifest" href="{{ asset('/manifest.json') }}">
</head>

<body>
    <nav class="navbar bg-light fixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('dashboard', $profile->nickname) }}">
                <i class="fas fa-arrow-left"> </i> BACK

            </a>
        </div>
    </nav>

    {{-- MODAL EDIT --}}
    <div class="modal fade" id="modal-edit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Post Description
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row h-100">
                        <form action="" method="post" id="edit-form-modal">
                            @csrf
                            @method('PUT')
                            <div class="form-outline mb-4">
                                <label class="form-label" for="textarea-desc">Description</label>
                                <textarea name="desc" class="form-control form-control-md pb-0" id="textarea-desc" rows="10"
                                    placeholder="Type a description for this post"></textarea>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success px-4" form="edit-form-modal">Edit</button>
                </div>
            </div>
        </div>
    </div>
    {{-- END MODAL EDIT --}}

    <div class="container min-vh-100 d-flex justify-content-center pt-2 mb-5">
        <div class="post-box px-5 mt-5 pt-4">
            @foreach ($posts as $item)
                <div class="post-item w-100 mb-5 " id="post{{ $item->id }}">
                    @if ($item->is_video)
                        <video class="w-100" controls>
                            <source src="{{ asset("img/posts/$item->thumbnail") }}" type="video/mp4">
                            Your browser does not support HTML video.
                        </video>
                    @else
                        <img class="w-100" src="{{ asset("img/posts/$item->thumbnail") }}" alt="Post1">
                    @endif
                    <p class="mb-0 fw-light text-end" style="font-size: .7rem;">
                        Work at
                        <span class="text-purple fw-bold">BURNINGROOM TECHNOLOGY</span>
                    </p>
                    <div class="post-description pt-3">
                        <div class="row justify-content-between">
                            <div class="col-8 d-flex">
                                <div class="pro-img">
                                    <img class="post-profil overflow-hidden rounded-circle"
                                        src="{{ asset("img/profiles/$profile->photo") }}" alt="joko"
                                        style="object-fit: cover;">
                                </div>

                                <div class="ms-3">
                                    <h5 class="pb-0 mb-0 namejob">{{ $profile->name }}
                                        <span class="fw-light">|
                                            {{ $profile->job }}
                                        </span>
                                    </h5>
                                    <p class="pt-0 mt-0 fw-light">{{ $item->created_at->format('d F Y') }}</p>
                                </div>
                            </div>
                            <div class="col-4 d-flex align-items-start justify-content-end">
                                <div class="btn-group dropstart">
                                    <button type="button" data-bs-toggle="dropdown" aria-expanded="false"
                                        class="border-0 bg-transparent">
                                        <i class="fa-solid fa-ellipsis-vertical"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a class="dropdown-item text-purple" href="#1" data-bs-toggle="modal"
                                                data-bs-target="#modal-edit" data-desc="{{ $item->desc }}"
                                                data-href="{{ route('update-post', $item->id) }}">
                                                <i class="fa-solid fa-pen pe-2"></i> Edit this Post
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item delete-post text-purple" href="#"
                                                data-href="{{ route('delete', $item->id) }}">
                                                <i class="fa-solid fa-trash pe-2"></i> Delete this Post
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <p>{{ $item->desc }}</p>
                    </div>
                    <hr>
                </div>
            @endforeach
        </div>
    </div>
    <form action="" method="POST" id="delete-form">
        @csrf
        @method('DELETE')
    </form>

    {{-- PWA --}}
    <script src="{{ asset('/sw.js') }}"></script>
    <script>
        if (!navigator.serviceWorker.controller) {
            navigator.serviceWorker.register("/sw.js").then(function(reg) {
                console.log("Service worker has been registered for scope: " + reg.scope);
            });
        }
    </script>
</body>

{{-- jquery --}}
<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $(".edit-post-btn").on("click", function(event) {
            $(".close-edit-modal").click();
            Swal.fire({
                icon: 'success',
                title: 'Success!',
                text: 'Your post has been edited!',
                showConfirmButton: false,
                timer: 2000
            }).then(function() {
                $("#edit-form").submit();
            });
        });

        $(".delete-post").on("click", function(event) {
            event.preventDefault();

            const href = $(this).data('href');
            const form = $('#delete-form');

            form.attr('action', href)

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-md btn-success mx-2',
                    cancelButton: 'btn btn-md btn-danger mx-2',
                },
                buttonsStyling: false
            });

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {
                    swalWithBootstrapButtons.fire({
                        title: 'Deleted!',
                        text: 'Your post has been deleted.',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 2000,
                    });
                    form.submit();
                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire({
                        title: 'Cancelled',
                        text: 'Your post is safe :v',
                        icon: 'error',
                        showConfirmButton: false,
                        timer: 2000,
                    })
                }
            });
        });

        const modal = $('#modal-edit')

        modal.on('show.bs.modal', event => {
            // Button that triggered the modal
            const button = event.relatedTarget

            const desc = button.getAttribute('data-desc')
            const href = button.getAttribute('data-href')
            // Update the modal's content.
            // const modalTitle = exampleModal.querySelector('.modal-title')
            const textarea = modal.find('#textarea-desc')
            const form = modal.find('#edit-form-modal')

            form.attr('action', href);

            textarea.html(desc)
            // modalBodyInput.value = recipient
        });
    });
</script>



</html>
