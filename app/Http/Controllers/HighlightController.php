<?php

namespace App\Http\Controllers;

use App\Models\Highlight;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use RealRashid\SweetAlert\Facades\Alert;

class HighlightController extends Controller
{
    use UploadFile;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'     => 'required',
            'thumbnail' => 'required|file|mimes:jpg,png,svg,mp4|max:10240',
            'desc'      => 'required',
        ]);
        // dd($request);

        $result = $this->_uploadFile($request->thumbnail, 'img/highlights');

        $highlight = new Highlight();

        if ($result['path']) {
            $highlight->thumbnail = $result['filename'];
        }

        $highlight->fill($request->except(['_token', 'thumbnail']));

        $highlight->user_id  = Auth::id();
        $highlight->title    = $request->title;
        $highlight->desc     = $request->desc;
        $highlight->is_video = $request->file('thumbnail')->extension() == 'mp4';

        $highlight->save();

        // Alert::success('Berhasil!', 'Data Telah Ditambahkan!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Highlight $highlight)
    {
        $request->validate([
            'title' => 'required',
            'desc' => 'required',
        ]);

        $highlight->desc = $request->desc;
        $highlight->title = $request->title;

        $highlight->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Highlight $highlight)
    {
        $highlight->delete();
        return back();
    }
}
