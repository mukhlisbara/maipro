<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Traits\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
    use UploadFile;
    public function showPhotos($nickname)
    {
        // dd('test');
        $profile = User::whereNickname($nickname)->first();
        $posts = $profile->posts()->photo()->get();

        return view('post', compact('posts', 'profile', 'nickname'));
    }

    public function showVideos($nickname)
    {
        // dd('test');
        $profile = User::whereNickname($nickname)->first();
        $posts = $profile->posts()->video()->get();

        return view('post', compact('posts', 'profile', 'nickname'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'thumbnail' => 'required|file|mimes:jpg,png,svg,mp4|max:10240',
            // 'desc'      => 'required',
        ]);
        // dd($request);

        $result = $this->_uploadFile($request->thumbnail, 'img/posts');

        $post = new Post();

        if ($result['path']) {
            $post->thumbnail = $result['filename'];
        }

        // $post->fill($request->except(['_token', 'thumbnail']));

        $post->user_id  = Auth::id();
        $post->desc     = $request->desc;
        $post->is_video = $request->is_video;

        $post->save();

        // Alert::success('Berhasil!', 'Data Telah Ditambahkan!');
        return back();
    }

    public function update(Request $request, Post $post)
    {
        $request->validate([
            'desc' => 'required',
        ]);

        $post->desc = $request->desc;

        $post->save();

        return back();
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return back();
    }
}
