<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Models\Highlight;
use App\Traits\UploadFile;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    use UploadFile;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     return view('home');
    // }

    public function index($nickname)
    {
        // dd('test');
        $profile = User::whereNickname($nickname)->first();
        // dd($profile->posts()->photo());

        $postfoto = $profile->posts()->photo()->get();
        $postvideo = $profile->posts()->video()->get();
        $posthighlight = $profile->highlights()->get();

        // $postfoto = Post::where('is_video', 0)->get();
        // $postvideo = Post::where('is_video', 1)->get();
        return view('dashboard', [
            'profile' => $profile,
            'postfoto' => $postfoto,
            'postvideo' => $postvideo,
            'posthighlight' => $posthighlight,
        ]);
    }

    public function update(Request $request, User $user)
    {
        // $request->validate([
        //     'nickname' => 'required',
        //     'name' => 'required',
        //     'photo' => 'required',
        //     'background' => 'required',
        //     'job' => 'required',
        //     'bio' => 'required',

        // ]);

        // $user->nickname = $request->nickname;
        // $user->name = $request->name;
        // $user->photo = $request->photo;
        // $user->background = $request->background;
        // $user->job = $request->job;
        // $user->bio = $request->bio;
        // dd($user);

        $user->fill($request->input());
        if ($request->photo) {
            $result = $this->_uploadFile($request->photo, 'img/profiles');
            if ($result['path']) {
                $user->photo = $result['filename'];
            }
        }
        if ($request->background) {
            $result = $this->_uploadFile($request->background, 'img/profiles');
            if ($result['path']) {
                $user->background = $result['filename'];
            }
        }

        $user->save();

        return back();
    }
}
