<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;


class Highlight extends Model
{
       use HasFactory;

       protected $guarded = [];

       protected static function boot()
       {
              parent::boot();

              static::deleting(function ($post) {
                     File::delete(public_path() . "/highlights/$post->thumbnail");
              });
       }

       public function user()
       {
              return $this->belongsTo(User::class);
       }

       public function scopePhoto($query)
       {
              return $query->where('is_video', 0);
       }
}
