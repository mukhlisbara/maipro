<?php

namespace Database\Seeders;

use Carbon\CarbonImmutable;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $datas = [];
        foreach ($users as $user) {

            // posts with photo
            for ($i = 1; $i <= 6; $i++) {
                $datas[] = [
                    'user_id'    => $user->id,
                    'thumbnail'  => "photo$i.jpg",
                    'desc'       => "POST $i, Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero voluptate, possimus, quasi temporibus itaque voluptas quos eos soluta dignissimos earum, non ducimus est! Illum inventore excepturi possimus est dicta quis suscipit quae a maxime magni voluptas culpa cum expedita aut, minus dolores voluptates, esse sapiente illo in eaque et beatae.",
                    'is_video'   => false,
                    'created_at' => CarbonImmutable::now()->subDays(rand(1, 5)),
                    'updated_at' => CarbonImmutable::now()->subDays(rand(1, 5)),
                    // 'highlighted' => false,
                ];
            }

            // posts with video
            $exts = ['mp4', 'ogv'];
            // for ($i = 1; $i <= 6; $i++) {
            for ($i = 1; $i <= 2; $i++) {
                $datas[] = [
                    'user_id'    => $user->id,
                    // 'thumbnail'  => "movie.".$exts[rand(0,1)],
                    'thumbnail'  => "movie$i.mp4",
                    'desc'       => "POST VIDEO $i, Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero voluptate, possimus, quasi temporibus itaque voluptas quos eos soluta dignissimos earum, non ducimus est! Illum inventore excepturi possimus est dicta quis suscipit quae a maxime magni voluptas culpa cum expedita aut, minus dolores voluptates, esse sapiente illo in eaque et beatae.",
                    'is_video'   => true,
                    'created_at' => CarbonImmutable::now()->subDays(rand(1, 5)),
                    'updated_at' => CarbonImmutable::now()->subDays(rand(1, 5)),
                    // 'highlighted' => false,
                ];
            }
        }

        Post::insert($datas);
    }
}
