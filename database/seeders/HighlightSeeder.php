<?php

namespace Database\Seeders;

use App\Models\Highlight;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HighlightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $datas = [];
        foreach ($users as $user) {
            for ($i = 1; $i <= 6; $i++) {
                $datas[] = [
                    'user_id'    => $user->id,
                    'title'      => "Judul photo$i.jpg",
                    'thumbnail'  => "photo$i.jpg",
                    'desc'       => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero voluptate, possimus, quasi temporibus itaque voluptas quos eos soluta dignissimos earum, non ducimus est! Illum inventore excepturi possimus est dicta quis suscipit quae a maxime magni voluptas culpa cum expedita aut, minus dolores voluptates, esse sapiente illo in eaque et beatae.',
                    'is_video'   => false,
                    'created_at' => CarbonImmutable::now()->subHours(rand(1, 24)),
                    'updated_at' => CarbonImmutable::now()->subHours(rand(1, 24)),
                    // 'highlighted' => false,
                ];
            }
            for ($i = 1; $i <= 2; $i++) {
                $datas[] = [
                    'user_id'    => $user->id,
                    'title'      => "Judul Video$i",
                    'thumbnail'  => "movie$i.mp4",
                    'desc'       => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero voluptate, possimus, quasi temporibus itaque voluptas quos eos soluta dignissimos earum, non ducimus est! Illum inventore excepturi possimus est dicta quis suscipit quae a maxime magni voluptas culpa cum expedita aut, minus dolores voluptates, esse sapiente illo in eaque et beatae.',
                    'is_video'   => true,
                    'created_at' => CarbonImmutable::now()->subHours(rand(1, 24)),
                    'updated_at' => CarbonImmutable::now()->subHours(rand(1, 24)),
                    // 'highlighted' => false,
                ];
            }
            $datas[] = [
                'user_id'    => $user->id,
                'title'      => "Judul Video",
                'thumbnail'  => "movie.mp4",
                'desc'       => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero voluptate, possimus, quasi temporibus itaque voluptas quos eos soluta dignissimos earum, non ducimus est! Illum inventore excepturi possimus est dicta quis suscipit quae a maxime magni voluptas culpa cum expedita aut, minus dolores voluptates, esse sapiente illo in eaque et beatae.',
                'is_video'   => true,
                'created_at' => CarbonImmutable::now()->subHours(rand(1, 24)),
                'updated_at' => CarbonImmutable::now()->subHours(rand(1, 24)),
            ];
        }

        Highlight::insert($datas);
    }
}
